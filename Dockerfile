FROM debian:trixie-slim

COPY resolvconf_1000_all.deb /tmp/

RUN set -ex; \
    env DEBIAN_FRONTEND=noninteractive apt -y update; \
    env DEBIAN_FRONTEND=noninteractive apt -y install --no-install-recommends apt-utils dirmngr apt-transport-https; \
    env DEBIAN_FRONTEND=noninteractive apt -y full-upgrade --no-install-recommends; \
    env DEBIAN_FRONTEND=noninteractive apt -y clean; \
    rm -rf /var/lib/apt/lists /var/cache/apt/archives

RUN set -ex; \
    env DEBIAN_FRONTEND=noninteractive apt -y update; \
    env DEBIAN_FRONTEND=noninteractive apt -y install --no-install-recommends firefox-esr zsh default-jre lsof strace psmisc sudo s6 execline gnupg rxvt-unicode xrdp xorgxrdp less wmaker locales iptables iproute2; \
    env DEBIAN_FRONTEND=noninteractive apt -y clean; \
    rm -rf /var/lib/apt/lists /var/cache/apt/archives

RUN set -ex; \
    echo deb https://repo.windscribe.com/debian buster main > /etc/apt/sources.list.d/windscribe-repo.list; \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-key FDC247B7; \
    env DEBIAN_FRONTEND=noninteractive apt -y update; \
    dpkg -i /tmp/resolvconf_1000_all.deb; \
    env DEBIAN_FRONTEND=noninteractive apt -y install --no-install-recommends windscribe-cli; \
    env DEBIAN_FRONTEND=noninteractive apt -y autoremove; \
    env DEBIAN_FRONTEND=noninteractive apt -y clean; \
    rm -rf /var/lib/apt/lists /var/cache/apt/archives /tmp/resolvconf_1000_all.deb

RUN set -ex; \
    echo '%sudo   ALL=(ALL:ALL) ALL' > /etc/sudoers.d/sudo-group; \
    chmod 440 /etc/sudoers.d/sudo-group; \
    useradd -c 'Downloader user' -m -G sudo -s /bin/zsh -U -u 1000 -p '' download

COPY --chown=1000:1000 /home/download/. /home/download/
COPY etc/. /etc/
RUN set -ex; \
    ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime; \
    locale-gen; \
    update-locale LANG=en_GB.UTF-8

ARG WINDSCRIBE_USERNAME
ARG WINDSCRIBE_PASSPHRASE
RUN set -ex; \
    if [ -z "$WINDSCRIBE_USERNAME" ]; then \
        echo 'Specify Windscribe username on docker build command line using --build-arg=WINDSCRIBE_USERNAME=<username>.' 1>&2; \
        exit 1; \
    elif [ -z "$WINDSCRIBE_PASSPHRASE" ]; then \
        echo "Specify passphrase for /etc/windscribe/credentials.txt.gpg on docker build command line using --build-arg=WINDSCRIBE_PASSPHRASE='<passphrase>'." 1>&2; \
        exit 2; \
    fi; \
    echo "$WINDSCRIBE_PASSPHRASE" | gpg --batch --yes --passphrase-fd 0 /etc/windscribe/credentials.txt.gpg; \
    shred /etc/windscribe/credentials.txt.gpg; \
    rm /etc/windscribe/credentials.txt.gpg; \
    openvpn_username="`head -1 /etc/windscribe/credentials.txt`"; \
    openvpn_password="`head -2 /etc/windscribe/credentials.txt | tail -1`"; \
    sed -iE "s/<OPENVPN_USERNAME>/$openvpn_username/g; s/<OPENVPN_PASSWORD>/$openvpn_password/g; s/<WINDSCRIBE_USERNAME>/${WINDSCRIBE_USERNAME}/g" /etc/windscribe/wssession; \
    sed -iE "s/<WINDSCRIBE_USERNAME>/${WINDSCRIBE_USERNAME}/g" /etc/windscribe/wssettings

ENV LESS=-FiRM
WORKDIR /home/download
ENTRYPOINT ["/usr/bin/s6-svscan", "/etc/services.d"]
